import React from 'react';
import { StyleSheet,Text,TouchableOpacity,View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default function TodoItem({ item ,pressHandler}){
    return (
        
            <View style={styles.item}>
                <Icon onPress={()=>pressHandler(item.key)} name="delete" size={20} color="#000000" />
                <Text style={styles.text} >{item.text}</Text>
            </View>
    )
}
const styles=StyleSheet.create({
    item:{
        padding:16,
        marginTop:16,
        borderColor:'#bbb',
        borderWidth:1,
        borderStyle:'dashed',
        borderRadius:10,
        flexDirection:'row'
    },
    text:{
            marginLeft:10
    }
})