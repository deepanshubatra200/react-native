import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";

export default function AddTodos({onsubmit}) {
    const [text,setText]=useState('')
  const changeHandler = (value) => {
      setText(value)
  };
  const submit=()=>{
    onsubmit(text);
    setText('')
  }
  return (
    <View>
      <TextInput style={styles.input} placeholder="New Todo ..." value={text} onChangeText={changeHandler} />
      <Button title="Add Todo" color="coral" onPress={()=>submit()}></Button>
    </View>
  );
}
const styles=StyleSheet.create({
    input:{
        marginBottom:10,
        paddingHorizontal:8,
        paddingVertical:6,
        borderBottomWidth:1,
        borderBottomColor:'#ddd'
    }
})
